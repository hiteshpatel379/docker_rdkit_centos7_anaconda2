
- To build the image from docker file:

	```
	docker build --rm -t hiteshpatel379/rdkit:rdkit_centos7_anaconda2 .
	```

- To work in mounted directory from host:

	```
	docker run --rm -it --name rdkit_centos7_anaconda2 --mount type=bind,source='~/Documents',target=/app hiteshpatel379/rdkit:rdkit_centos7_anaconda2
	```

- To work in Jupyter-notebook:
	Run it like:

	```
	docker run --rm -ti --name rdkit_centos7_anaconda2 -p 8888:8888 hiteshpatel379/rdkit:rdkit_centos7_anaconda2
	```
	
	Start Jupyter-notebook:
	
	```
	jupyter-notebook --ip=0.0.0.0 --port=8888 --allow-root
	```

