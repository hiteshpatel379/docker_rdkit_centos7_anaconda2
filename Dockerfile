
FROM centos:centos7

MAINTAINER Hitesh Patel "hiteshpatel379@gmail.com"

RUN \
    yum update -y && \
    yum install wget -y && \
    yum install tar -y && \
    yum groupinstall "Development tools" -y &&\
    yum install -y fontconfig libXrender libXext &&\
    cd /root/

RUN wget https://repo.continuum.io/archive/Anaconda2-5.0.1-Linux-x86_64.sh

RUN bash Anaconda2-5.0.1-Linux-x86_64.sh -b -p ~/anaconda &&\
    echo 'export PATH="~/anaconda/bin:$PATH"' >> ~/.bashrc &&\
    source ~/.bashrc &&\
    conda update conda &&\
    conda install -y -c rdkit rdkit &&\
    conda install -y -c rdkit postgresql

